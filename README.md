## Životopis

Původně jsem backend programátor s dlouholetými zkušenostmi na rozličných
projektech a to jak zaměřením (marketing, cms, platební systémy, interní
systémy, prezentace), tak velikostí (od malých po velké robustní aplikace
s velkým množstvím dat či velkou zátěží). Mám rád automatizaci, čistý kód a
logické uvažování. Do projektu umím vnést nejen své programátorské zkušenosti,
ale zároveň jsem schopný vytvořit i jeho kompletní analýzu a navrhnout vhodné
technologie a postupy. V poslední době mě sice okouzlil svět devops, nejvíce mě ale
teď baví vedení lidí a navrhování komplexních systémů. 

#### Osobní údaje
**Jméno:** Marek Kouřimský  
**Datum narození:** 6.5.1985  
**Email:** kourim (at) centrum.cz  
**Linkedin:** https://www.linkedin.com/in/marekkourimsky/

#### Vzdělání

> (2005 - 2009)  
> **ČVUT Praha**, fakulta elektrotechnická, obor výpočetní technika   
> 7 semestrů, studium ukončeno před bakalářskou zkouškou z rodinných důvodů

> (2004 - 2005)  
> **Univerzita Karlova**, Matematicko-fyzikální fakulta, obor informatika  
> 1 semestr

> (1996 - 2004)  
> **Gymnázium Opatov** , Konstantinova 1500, Praha 4  
> osmileté gymnázium, stud. zaměření - informatika a výpočetní technika,
> programování, maturnitní zkouška

#### Pracovní zkušenosti
> (2015 - současnost)  
> **Na volné noze**  
> **PHP a Python programátor**  
> Ve volném čase se starám o menší projekty, které potřebují mou péči. Většinou
> se jedná o rozsahem menší cms platformy, eshopy nebo webové prezentace. Dále
> pomáhám konzultovat architekturu aplikací, devops, clean code a nastavování
> firemních procesů a technologií.

> (2018 - současnost)  
> **CZECH NEWS CENTER a.s.**  
> **Vedoucí vývoje**  
> Aktuálně jsem zodpovědný za vedení vývoje v CNC. Mám na starost vedení sedmi týmů (cca 45 lidí),
> udávám technologický směr onlinu a architekturu celého řešení. Mezi další věci patří například
> nábor lidí, interní procesy a komunikace s obchodními partnery.

> (2017 - 2018)  
> **CZECH NEWS CENTER a.s.**  
> **PHP programátor**  
> Pomáhal jsem vylepšovat portfolio webů v CNC. Staral jsem se o architekturu
> aplikací, devops a clean code. Nastavoval jsem a vylepšoval jsem agilní týmové procesy.

> (2015 - 2016)  
> **Skrz.cz**  
> **PHP programátor**  
> Rok jsem pomáhal vylepšovat, zrychlovat a posouvat dále portál skrz.cz.

> (2009 - 2015)  
> **GrandIT s.r.o.**  
> **PHP a Python programátor**  
> Vývoj a údržba platebního portálu MPPort (platby přes telefon), aplikace
> SMS/MMS Info (zpracování a rozesílání informačních sms/mms), bývalého aukčního
> portálu Vydražsi.cz, interních systémů, distribuce digitálního obsahu (např.
> pro Alza.cz) a několika dalších portálů(např O2 web).

> (2007 - 2008)  
> **PLUS-Discount spol. s.r.o.**  
> **Web administrátor**  
> Administrace webu, příprava obsahu, jeho aktualizace.

#### Znalosti

###### Programovací jazyky
* PHP
* Python
* GOLANG (zkušenost s menšími projekty či menšími službami)
* C/C++/C# (zkušenosti primárně ze studií)
* bash (primárně pro devops tooly)
* javascript + css (zkušenosti primárně z menších projektů)

###### Databáze
* MySQL/PostgreSQL
* redis, memcache
* RabitMQ
* Elasticsearch

###### Verzovací systémy
* SVN
* GIT

###### DevOps
* Gitlab a Bitbucket CI/CD
* Docker / Swarm
* Kubernetes
* Prometheus / Grafana
* Rancher

###### Ostatní
* zkušenosti s frameworky Symfony, Nette, Bootstrap, jQuery
* api jako SOAP, RPC, REST a GraphQL
* composer, pip, npm jako package manager
* administrátorské zkušenosti s Windows i Ubuntu/Debian stroji, Ansible
* agilní metodiky vývoje

#### Ostatní
* Anglický jazyk - mírně pokročilý
* řidičský průkaz skupiny B
* analytické myšlení, důslednost, týmový hráč, ochota učit se nové věci